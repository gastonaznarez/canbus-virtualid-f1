#include <stm32f10x.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>

void gpio_init(void);
void toggle_led(void);
void set_led(int stat);
void toggle_pin(void);
void set_pin(int stat);
