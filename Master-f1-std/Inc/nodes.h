#include "stm32f10x_conf.h"
#define TABLE_SIZE 6

/* 
       id_table:
    +------+------+------+------+-----+
    | v_id |           f_id           |
    +------+------+------+------+-----+
*/

// Initialize the table structures
void nodetable_init(void);
/* Add node to the table
   Param[0] ---> uint32* f_id[4] Physical Id of the new node.
   Return:  -+-> -1 on bad argument or list full.
             |   
             +->  new v_id if the node has been successful added.
*/
uint16_t add_node(uint32_t* f_id);

uint16_t get_nextid(uint32_t* f_id);

/* Find a node in the table by Physical Id
   Pram[0] ---> Physical Id.
   Return: -+-> -1 if the node doesn't exists.
            |
            +-> The v_id if the node exists.
*/
uint16_t pfind_node(uint32_t* f_id);

uint16_t get_totalnodes(void);
