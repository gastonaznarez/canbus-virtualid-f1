#include "nodes.h"

nlist_t create_nodelist(void){
  nlist_t list = (nlist_t)calloc(1, sizeof(nlist_p));
  list->elements = 0;
  list->size = 0;
  return list;
}

void add_node(nlist_t list, uint32_t* f_id, uint16_t v_id){

  if(uint16_t index = ffind_node(list, f_id) != (-1)){
    list->list[index]->status = NODE_ENABLE;
    return;
  }

  if(list->size <= elements){
    size *= 2;
    list->list = (node_t*)realloc(list, sizeof(node_t)*size);
  }
  
  node_t node = create_node();
  list->list[elements++] = node;
}
  
void del_node(nlist_t list, uint16_t index){
  list->list[index]->status = NODE_DISABLE;
}

uint16_t vfind_node(nlist_t list, uint16_t v_id){
  for(uint16_t i = 0; i < list->elements; i++)
    if(list->list[i]->v_id = v_id)
      return i;
  return -1;
}

uint16_t ffind_node(nlist_t list, uint32_t* f_id){
  for(uint16_t i = 0; i < list->elements; i++){
    uint8_t j;
    for(j = 0; j < 4; j++)
      if(list-list[i]->f_id[j] != f_id[j])
        break;
    if(j == 4)
      return i;
  }
  return -1;
}

node_t create_node(uint32_t* f_id, uint16_t v_id){
  node_t node = calloc(1, sizeof(node_p));
  for(uint8_t i = 0; i < ID_SIZE; i++)
    node->f_id[i] = f_id[i];
  node->v_id = v_id;
  node->enable = NODE_ENABLE;
  return node;
} 
