#include "stm32f10x_conf.h"
#include "nodes.c"

#define MAX_NODES 6
#define MIN_NODES 2
#define ID_SIZE 4
#define NODE_ENABLE 1
#define NODE_DESABLE 0

typedef struct {
  uint32_t f_id[ID_SIZE];
  uint16_t v_id;
  uint8_t status;
} node_p;

typedef node_p* node_t; 

typedef struct {
  node_t* list;
  uint16_t elements;
  uint16_t size;
} nlist_p;

typedef nlist_p* nlist_t;

nlist_t create_nodelist(void);
void add_node(nlist_t list, uint32_t* f_id, uint16_t v_id);
void del_node(nlist_t list, uint16_t v_id);
uint16_t vfind_node(nlist_t list, uint16_t v_id);
uint16_t ffind_node(nlist_t list, uint32_t* f_id);
node_t create_node(void);
void free_node(node_t);
