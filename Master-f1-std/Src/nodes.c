#include "nodes.h"

uint32_t id_table[5][TABLE_SIZE];
uint16_t total_nodes; 

void nodetable_init(){
  total_nodes = 0;
  for(uint16_t i = 0; i < TABLE_SIZE; i++){
    for(uint8_t j = 1; j < 5; j++)
      id_table[j][i] = 0;
    id_table[0][i] = i+1;
  }    
}

uint16_t add_node(uint32_t* f_id){
  
  uint16_t v_id = pfind_node(f_id);
  if(v_id) return v_id;

  if(total_nodes == TABLE_SIZE) return -1;
    
  for(uint8_t i = 1; i < 5; i++)
    id_table[i][total_nodes] = f_id[i-1];
  
  return id_table[0][total_nodes++];
}

uint16_t pfind_node(uint32_t* f_id){
 
  for(uint16_t i = 0; i < total_nodes; i++){
    uint8_t count = 0;
    while(count < 4 && f_id[count++] == id_table[count][i]);
    if(count == 4) return id_table[0][i];
  }  
  return 0;
}

uint16_t get_nextid(uint32_t* f_id){
  uint16_t v_id = pfind_node(f_id);
  if(v_id) return v_id;
 
  return total_nodes + 1;
}

uint16_t get_totalnodes(){
  return total_nodes;
}
