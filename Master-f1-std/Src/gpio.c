#include "gpio.h"

void gpio_init(void){
    // GPIO structure for port initialization
    GPIO_InitTypeDef GPIO_InitStructure;

    // enable clock on APB2
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,  ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,  ENABLE);

    // configure port A1 for driving an LED
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;    // output push-pull mode
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;   // highest speed
    GPIO_Init(GPIOC, &GPIO_InitStructure) ;             // initialize port

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;   // highest speed
    GPIO_Init(GPIOA, &GPIO_InitStructure) ;             // initialize port


}

void toggle_led(void) {
    set_led(!GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13));
}

void set_led(int stat) {
    if(stat) 
        GPIO_SetBits(GPIOC, GPIO_Pin_13);    // turn the LED on
    else
        GPIO_ResetBits(GPIOC, GPIO_Pin_13);    // turn the LED on
}

void set_pin(int stat) {
    if(stat) 
        GPIO_SetBits(GPIOA, GPIO_Pin_1);    // turn the LED on
    else
        GPIO_ResetBits(GPIOA, GPIO_Pin_1);    // turn the LED on
}

void toggle_pin(void) {
  set_pin(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_1));
}

