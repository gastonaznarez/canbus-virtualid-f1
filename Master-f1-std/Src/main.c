#include "main.h"

//#define NODE_1
#define CAN_BAUDRATE  1000

void delay(int millis) {
  while (millis-- > 0) {
      volatile int x = 10000;
      while (x-- > 0) {
          __asm("nop");
      }
  }
}

int main(void) {
  gpio_init();
  timer_config(); 
  CAN_Config();
  NVIC_Config();
  delay(DELAY*2);
  network_init();

  uint8_t data_list[8] = {0, 0, 0, 0, 0, 0, 0, 0};

  //TIM_Cmd(TIM2, ENABLE);
  // main loop
  while(1) {
    toggle_led();
    send_idreq(0, 0);
    uint16_t size = get_totalnodes();
    if(!size) delay(DELAY);
    for(int i = 0; i < size; i++){ 
      delay(DELAY/2);
      send_to(i+1, 0, data_list); 
    }
    /*
    for(uint8_t i = 0; i < 16; i++){
      if( node_list[i].v_id ){
        send_to(node_list[i].v_id, 0, data_list); 
        delay(DELAY/2);
        toggle_led();
      }
    }
    */
    //toggle_led();
    //toggle_pin();
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif
