#include "can.h"

CAN_InitTypeDef         CAN_InitStructure;
CAN_FilterInitTypeDef   CAN_FilterInitStructure;
CanTxMsg                Msg_std;
CanTxMsg                Msg_ext;
unsigned int counter = 0;

void CAN_Config(void) {
  GPIO_InitTypeDef  GPIO_InitStructure;

  // GPIOD bus is alredy initialiced
  /* GPIOA, GPIOD and AFIO clocks enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | 
                         RCC_APB2Periph_GPIOA, 
                         ENABLE);

  /* Configure CAN1 RX pin */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
   
  /* Configure CAN1 TX pin */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Remaps of CAN1 GPIOs 
  +----------+---------+---------+---------+
  | Function | Remap_0 | Remap_1 | Remap_2 |
  +----------+---------+---------+---------+
  | CAN1_Rx  |   PA11  |   PB8   |   PD0   |
  | CAN1_Tx  |   PA12  |   PB9   |   PD1   |
  +----------+---------+---------+---------+

  GPIO_PinRemapConfig(GPIO_Remap_CAN1 , ENABLE);  
*/

  /* CAN1 Periph clocks enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);  
  
   /* CAN1 register init */
  CAN_DeInit(CAN1);

  /* Struct init*/
  CAN_StructInit(&CAN_InitStructure);

  /* CAN1 cell init */
  CAN_InitStructure.CAN_TTCM = DISABLE;
  CAN_InitStructure.CAN_ABOM = DISABLE;
  CAN_InitStructure.CAN_AWUM = DISABLE;
  CAN_InitStructure.CAN_NART = ENABLE;
  CAN_InitStructure.CAN_RFLM = DISABLE;
  CAN_InitStructure.CAN_TXFP = DISABLE;
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
  CAN_InitStructure.CAN_SJW = CAN_SJW_1tq; 
  CAN_InitStructure.CAN_BS1 = CAN_BS1_3tq;
  CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;
 
  CAN_InitStructure.CAN_Prescaler = 48; //125KBps

/*
+--------------+---------------+
| CAN_BAUDRATE | CAN_Prescaler |
+--------------+---------------+
|     1000     |       6KBps   |
|      500     |      12KBps   |
|      250     |      24KBps   |
|      125     |      48KBps   |
|      100     |      60KBps   |
|       50     |     120KBps   |
|       20     |     300KBps   |
|       10     |     600KBps   |
+--------------+---------------+
*/

  /*Initializes the CAN1  and CAN2 */
  CAN_Init(CAN1, &CAN_InitStructure);

  /* CAN1 filter init */
  CAN_FilterInitStructure.CAN_FilterNumber = 2;
  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = 0x000;
  CAN_FilterInitStructure.CAN_FilterIdLow = 0x000;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x000;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x000;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO0;
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  
  CAN_FilterInit(&CAN_FilterInitStructure);

  CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

  /* Transmit */
  Msg_std.StdId = 0x123;
  Msg_std.ExtId = 0x1AA;
  Msg_std.RTR = CAN_RTR_DATA;
  Msg_std.IDE = CAN_ID_STD;
  Msg_std.DLC = 1;  
  Msg_std.Data[0] = 0xAA;

  Msg_ext.StdId = 0x123;
  Msg_ext.ExtId = 0x34567;
  Msg_ext.RTR = CAN_RTR_REMOTE;
  Msg_ext.IDE = CAN_ID_EXT;
  Msg_ext.DLC = 0;  

}

void send_flag(void) {
  if(counter++ % 2) {
    Msg_std.Data[0] = counter;
    CAN_Transmit(CAN1, &Msg_std);
  } else {
    Msg_ext.Data[0] = counter;
    CAN_Transmit(CAN1, &Msg_ext);
  }
}
