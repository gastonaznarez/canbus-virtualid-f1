#include "network.h"

uint32_t* id_fragment;  // Assignment id
uint8_t net_status = 0;                  // Id assginment position
CanTxMsg NetTxMsg;                       // Transmit msg for networking

void send_to(uint16_t id, uint8_t dlc, uint8_t* data){
  NetTxMsg.IDE = CAN_ID_STD;
  NetTxMsg.StdId = id;
  NetTxMsg.RTR = CAN_RTR_DATA;
  NetTxMsg.DLC = dlc;
  for(uint8_t i = 0; i < dlc; i++)
    NetTxMsg.Data[i] = data[i];
  CAN_Transmit(CAN1, &NetTxMsg);
}

void network_init(void){

  id_fragment = (uint32_t*)calloc(4, sizeof(uint32_t));

  nodetable_init();

  NetTxMsg.StdId = 0x000;
  NetTxMsg.ExtId = 0x000;
  NetTxMsg.RTR = CAN_RTR_DATA;
  NetTxMsg.IDE = CAN_ID_EXT;
  NetTxMsg.DLC = 0; 
  for(uint8_t i = 0; i < 8; i++)
     NetTxMsg.Data[i] = 0x0;

  send_idreq(0, 0);

}

void send_idreq(uint8_t pos, uint32_t id){
  // send msg with the ExtId {00|pos|id}
  NetTxMsg.ExtId = (pos << 24) | id;
  NetTxMsg.RTR = CAN_RTR_DATA;
  NetTxMsg.IDE = CAN_ID_EXT;
  NetTxMsg.DLC = 0; 
  for(uint8_t i = 0; i < 8; i++)
     NetTxMsg.Data[i] = 0x0;
  CAN_Transmit(CAN1, &NetTxMsg);
}

void id_confirm(uint16_t in_id){
  // Set status to 0
  net_status = 0;
  // Save the new node
  uint16_t id = add_node(id_fragment);
  toggle_led();
  send_idreq(7, id);
  //Continue with the assignment
  send_idreq(0, 0);
}

void handle_networking(CanRxMsg* msg){
  // Check that the msg correspond to node-id assign
  // Check if the first two bits of the message are 0
  if(!FROM_SLAVE(msg->ExtId)) return;
  uint8_t position = POS_MASK(msg->ExtId);
  // Check if the position is an old msg
  if(position != net_status) return;
  uint32_t in_id = msg->ExtId & 0xFFFFFF;
  
  switch(position){
    case 3:
      // Send new virtual-id
      net_status++;
      send_idreq(position + 1, get_nextid(id_fragment));
      break;
    case 4:
      //Confirm the virtual-id assignment
      id_confirm(in_id);
      break;
    case 0:
    case 1:
    case 2:
      //Save the id fragment and continue
      net_status++;
      id_fragment[position] = in_id;
      send_idreq(position + 1, in_id);
      break;
  }  
}

void handle_message(CanRxMsg* msg) {
  // Check if the msg has standard or extended id format
  switch(msg->IDE){
    case CAN_ID_EXT:
      handle_networking(msg);
      break;
    case CAN_ID_STD:
      //Is MIDI or not?
      if(msg->StdId == 0) toggle_led();
      break;
  }
}

