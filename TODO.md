# MASTER #

[x] Send msg to all nodes one by one.
[x] Recognize when a node with virtual id want other id and give the same.
[ ] Send msg to restart the id assignment on reboot.
[ ] Make library.
[ ] Export to the f4.

-------------------------------------------------------------------

# SLAVE #

[ ] Check posible errors.
[ ] Make more robust.
[x] Recognize msgs with his own id.
[ ] Make on-the-fly filters with his id.
[ ] Recognize when the master was rebooted.

-------------------------------------------------------------------

# NETWORK #

[ ] Check the network with more than 3 nodes.
[ ] Check desconection of a single and multiples nodes.
[ ] Check the desconection of the master.

-------------------------------------------------------------------
