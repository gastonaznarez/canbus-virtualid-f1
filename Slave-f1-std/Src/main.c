#include "main.h"

//#define NODE_1
#define CAN_BAUDRATE  1000

void delay(int millis) {
  while (millis-- > 0) {
      volatile int x = 10000;
      while (x-- > 0) {
          __asm("nop");
      }
  }
}

int main(void) {
  gpio_init();
  timer_config(); 
  CAN_Config();
  NVIC_Config();
  toggle_led();
  network_init();
  // main loop
  while(1) {
    delay(DELAY*2);
    //toggle_led();
    //toggle_pin();
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}

#endif
