#include "network.h"

uint32_t f_id[4] = { 0x333333, 0x333333, 0xBBBBBB, 0x345678 };
uint16_t v_id = 0;
uint8_t net_status = 0;
CanTxMsg NetTxMsg;


void network_init(void){
  NetTxMsg.StdId = 0x000;
  NetTxMsg.ExtId = 0x000;
  NetTxMsg.RTR = CAN_RTR_DATA;
  NetTxMsg.IDE = CAN_ID_EXT;
  NetTxMsg.DLC = 0; 
  for(uint8_t i = 0; i < 8; i++)
     NetTxMsg.Data[i] = 0x0;
}

void send_idres(uint8_t pos, uint32_t id){
  NetTxMsg.ExtId = (1 << 27 | pos << 24 | id);
  CAN_Transmit(CAN1, &NetTxMsg);
}

void id_confirm(uint16_t in_id){
  // Set status to 0
  net_status = 0;
  // Save the new virtual id
  v_id = in_id;
  toggle_led();
}

void handle_networking(CanRxMsg* msg){
  // Check that the msg correspond to node-id assign
  // Check if the message is from the master
  if(msg->ExtId >> 27) return;
  // Check if the position is an old msg
  // if happen then, we are out, try again
  uint8_t position = (msg->ExtId >> 24) & 0x7;
  if(position != net_status){ 
    net_status = 0;
    return;
  }
  uint32_t in_id = msg->ExtId & 0xFFFFFF;
  
  switch(position){
    case 0:
      // If is the first message check that the id is 0
      if(in_id) break;
      send_idres(position, f_id[position]);
      net_status++;
      break;
    case 4:
      //Confirm the virtual-id assignment
      id_confirm(in_id);
      send_idres(position, in_id);
      break;
    case 1:
    case 2:
    case 3:
      //Save the id fragment and continue
      if(in_id != f_id[position-1]) break; 
      send_idres(position, f_id[position]);
      net_status++;
      break;
  }  
}

void handle_message(CanRxMsg* msg) {
  // Check if the msg has standard or extended id format 
  switch(msg->IDE){
    case CAN_ID_EXT:
      if(!v_id) handle_networking(msg);
      break;
    case CAN_ID_STD:
      //Is MIDI or not?
      if(v_id && (msg->StdId & 0x1FF) == v_id) toggle_led(); 
      break;
  }
}
