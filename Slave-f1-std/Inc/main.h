#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f10x.h"
#include "gpio.h"
#include "timer.h"
#include "nvic.h"
#include "can.h"
#include "stm32f10x_conf.h"
#include "network.h"

#define DELAY 1500  // in millis
#define SYSCLK_FREQ_72MHz

#endif
