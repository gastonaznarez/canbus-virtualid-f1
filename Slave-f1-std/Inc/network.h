// Imports
#include "can.h"
#include "stm32f10x_conf.h"
#include "gpio.h"

// Messages definition

// From-To
#define FMB  (0x0 << 9)   // From Master Broadcast
#define FMTS (0x1 << 9)   // From Master To Slave
#define FSTM (0x2 << 9)	  // From Slave To Master
#define FSTS (0x3 << 9)   // From Slave To Slave

// Masks
#define POS_MASK(id) (0xF & (id >> 24))

// Functions declarations
void network_init(void);
void send_idreq(uint8_t pos, uint32_t id);
void id_confirm(uint16_t in_id);
void handle_networking(CanRxMsg* msg);
void handle_message(CanRxMsg* msg) ;

