# path to STM32F103 standard peripheral library
STD_PERIPH_LIBS ?= ./Libraries/STM32F10x_StdPeriph_Lib_V3.5.0/

# list of source files
SOURCES  = Src/*
SOURCES += $(STD_PERIPH_LIBS)/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/system_stm32f10x.c
SOURCES += $(STD_PERIPH_LIBS)/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_rcc.c
SOURCES += $(STD_PERIPH_LIBS)/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_gpio.c
SOURCES += $(STD_PERIPH_LIBS)/Libraries/STM32F10x_StdPeriph_Driver/src/stm32f10x_can.c
SOURCES += $(STD_PERIPH_LIBS)/Libraries/STM32F10x_StdPeriph_Driver/src/*
SOURCES += $(STD_PERIPH_LIBS)/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/startup/TrueSTUDIO/startup_stm32f10x_md.s

# name for output binary files
PROJECT ?= CanBus-IdSender
COMP_BASE ?= build/

# compiler, objcopy (should be in PATH)
CC = arm-none-eabi-gcc
OBJCOPY = arm-none-eabi-objcopy

# path to st-flash (or should be specified in PATH)
ST_FLASH ?= st-flash

# specify compiler flags
CFLAGS  = -g -O2 -Wall
CFLAGS += -T$(STD_PERIPH_LIBS)/Project/STM32F10x_StdPeriph_Template/TrueSTUDIO/STM3210B-EVAL/stm32_flash.ld
CFLAGS += -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork
CFLAGS += -mfloat-abi=hard -mfpu=fpv4-sp-d16
CFLAGS += -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER
CFLAGS += -Wl,--gc-sections
CFLAGS += -I.
CFLAGS += -I$(STD_PERIPH_LIBS)/Libraries/CMSIS/CM3/DeviceSupport/ST/STM32F10x/
CFLAGS += -I$(STD_PERIPH_LIBS)/Libraries/CMSIS/CM3/CoreSupport
CFLAGS += -I$(STD_PERIPH_LIBS)/Libraries/STM32F10x_StdPeriph_Driver/inc
CFLAGS += -I Inc/

OBJS = $(SOURCES:.c=.o)

all: $(COMP_BASE) $(PROJECT).elf

$(COMP_BASE):
	mkdir $(COMP_BASE)

# compile
$(PROJECT).elf: $(SOURCES)
	$(CC) $(CFLAGS) $^ -o $(COMP_BASE)$@
	$(OBJCOPY) -O ihex $(COMP_BASE)$(PROJECT).elf $(COMP_BASE)$(PROJECT).hex
	$(OBJCOPY) -O binary $(COMP_BASE)$(PROJECT).elf $(COMP_BASE)$(PROJECT).bin

# remove binary files
clean:
	rm -rf *.o *.elf *.hex *.bin build/

# debug
debug:
	make
	arm-none-eabi-gdb $(COMP_BASE)$(PROJECT).elf

# flash
flash:
	make && $(ST_FLASH) write $(COMP_BASE)$(PROJECT).bin 0x8000000
